﻿using System.Collections;
using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;


//var Aiplane = new Dictionary<string, string>();

namespace Assets.src
{
    public class Brigde : MonoBehaviour
    {

        public struct Airplane
        {
            public string callsign { get; set; }
            public string mode { get; set; }
            public float lat { get; set; }
            public float lon { get; set; }
            public int alt { get; set; }
            public int gs { get; set; }
            public string type { get; set; }
            public int squawk { get; set; }
            public string flight_plan { get; set; }
            public string IorV { get; set; }
            public string origin { get; set; }
            public string destination { get; set; }
            public int heading { get; set; }
        }

        // Use this for initialization
        void Start()
        {
            Console.WriteLine("Hello World!");
            Debug.Log("Hello World!");

        }
        // Update is called once per frame
        void Update()
        {
            var airplanes = GetAirplanes();
            foreach (Airplane airplane in airplanes)
            {
                    Debug.Log(airplane.callsign);
            }
                Thread.Sleep(3000);

        }

        List<Airplane> GetAirplanes()
        {
            var Airplanes = new List<Airplane>();

            string whazzupPATH = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\JoinFS\Test.txt";

            var lines = File.ReadAllLines(whazzupPATH);

            int num_clients = 0;

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("CONNECTED CLIENTS"))
                {
                    var split_line = lines[i].Split(' ');
                    num_clients = int.Parse(split_line[3]);
                    break;
                }
            }
            var client_data_list = new List<string>();
            bool started = false;

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].StartsWith("!CLIENTS"))
                {
                    started = true;
                }
                else if (lines[i].StartsWith("!SERVERS"))
                {
                    break;
                }
                else if (started)
                {
                    client_data_list.Add(lines[i]);
                    Console.WriteLine(lines[i]);
                }
            }
            foreach (var client_data in client_data_list)
            {
                var split_data = client_data.Split(':');
                if (split_data[3] == "ATC")
                {
                    continue;
                }
                else if (split_data[3] == "PILOT")
                {
                    var callsign = split_data[0];
                    var mode = split_data[3];
                    var lat = float.Parse(split_data[5]);
                    var lon = float.Parse(split_data[6]);
                    var alt = int.Parse(split_data[7]);
                    var gs = int.Parse(split_data[8]);
                    var type = split_data[9];
                    var origin = split_data[11];
                    var destination = split_data[13];
                    var squawk = int.Parse(split_data[17]);
                    var IorV = split_data[21]; // IFRorVFR
                    var heading = int.Parse(split_data[38]);
                    Console.WriteLine(callsign);

                    string flight_plan;
                    if (IorV == "VFR")
                    {
                        Debug.Log("VFRで飛んでいます");
                        flight_plan = "VFR";
                    }
                    else if (IorV == "IFR")
                    {
                        Debug.Log("IFRで飛んでいます");
                        flight_plan = split_data[30];
                        Debug.Log(flight_plan);
                    }
                    else
                    {
                        Debug.Log("エラーが発生しました");
                        flight_plan = "";
                    }
                    Airplane airplane = new Airplane
                    {
                        callsign = callsign,
                        mode = mode,
                        lat = lat,
                        lon = lon,
                        alt = alt,
                        gs = gs,
                        type = type,
                        origin = origin,
                        destination = destination,
                        flight_plan = flight_plan,
                        squawk = squawk,
                        IorV = IorV,
                        heading = heading,
                    };
                    Airplanes.Add(airplane);
                    Debug.Log(airplane.alt);
                }
            }
            foreach (var airplane_data in Airplanes)
            {
                Debug.Log(Airplanes);
            }
            return Airplanes;
        }
    }
}