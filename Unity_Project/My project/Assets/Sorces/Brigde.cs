using System.Collections;
using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;

namespace Assets.src
{
	public class Brigde : MonoBehaviour
	{

		public struct Airplane
		{
			public string callsign { get; set; }
			public string mode { get; set; }
			public double lat { get; set; }
			public double lon { get; set; }
			public int alt { get; set; }
			public int gs { get; set; }
			public string type { get; set; }
			public int squawk { get; set; }
			public string flight_plan { get; set; }
			public string IorV { get; set; }
			public string origin { get; set; }
			public string destination { get; set; }
			public int heading { get; set; }
			public string remarks { get; set; }
		}

		// Use this for initialization
		void Start()
		{
			Console.WriteLine("Hello World!");
			Debug.Log("Hello World!");
		}

		// Update is called once per frame
		void Update()
		{

		}
        public String[] Open_Whazzup()
        {
            string whazzupPATH = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\JoinFS\Test.txt";
            var lines = File.ReadAllLines(whazzupPATH);

            return lines;
        }

        public List<Airplane> GetAirplanes()
		{
			var Airplanes = new List<Airplane>();
			var lines = Open_Whazzup();

			int num_clients = Get_num_clients(lines);

			List<string> client_data_list = Get_client_data(lines);

			Get_num_PILOT(lines,client_data_list);

			

			foreach (var client_data in client_data_list)
			{
				var split_data = client_data.Split(':');
				if (split_data[3] == "PILOT")
				{
					//Debug.Log("現在の接続されているパイロットの数:" + num_clients);

					var callsign = split_data[0];               // コールサイン
					var mode = split_data[3];                   // ATC or PILOT
					var lat = double.Parse(split_data[5]);      // 緯度
					var lon = double.Parse(split_data[6]);      // 経度
					var alt = int.Parse(split_data[7]);         // 高度
					var gs = int.Parse(split_data[8]);          // 対地速度
					var type = split_data[9];                   // 機体型
					var origin = split_data[11];                // 出発空港
					var destination = split_data[13];           // 到着空港
					var squawk = int.Parse(split_data[17]);     // スコークコード
					var IorV = split_data[21];                  // IFRorVFR
					var remarks = split_data[30];               // 特記事項
					var heading = int.Parse(split_data[38]);    // 方位
					Console.WriteLine(callsign);

					string flight_plan;
					if (IorV == "VFR")
					{
						//Debug.Log("VFRで飛んでいます");
						flight_plan = "VFR";
					}
					else if (IorV == "IFR")
					{
						//Debug.Log("IFRで飛んでいます");
						flight_plan = split_data[30];
						//Debug.Log(flight_plan);
					}
					else
					{
						Debug.Log("エラーが発生しました");
						flight_plan = "";
					}
					Airplane airplane = new Airplane
					{
						callsign = callsign,
						mode = mode,
						lat = lat,
						lon = lon,
						alt = alt,
						gs = gs,
						type = type,
						origin = origin,
						destination = destination,
						flight_plan = flight_plan,
						remarks = remarks,
						squawk = squawk,
						IorV = IorV,
						heading = heading,
					};
					Airplanes.Add(airplane);
					//Debug.Log(airplane.alt);
				}
			}
			return Airplanes;
		}

		public int Get_num_clients(String[] lines)
        {
			int num_clients = 0;
			for (int i = 0; i < lines.Length; i++)
			{
				if (lines[i].Contains("CONNECTED CLIENTS"))
				{
					var split_line = lines[i].Split(' ');
					num_clients = int.Parse(split_line[3]);
					break;
				}
			}
			return num_clients;
		}

		public List<string> Get_client_data(String[] lines)
        {
			var client_data_list = new List<string>();
			bool started = false;

			for (int i = 0; i < lines.Length; i++)
			{
				if (lines[i].StartsWith("!CLIENTS"))
				{
					started = true;
				}
				else if (lines[i].StartsWith("!SERVERS"))
				{
					break;
				}
				else if (started)
				{
					client_data_list.Add(lines[i]);
				}
			}
			return client_data_list;
		}

		public int Get_num_PILOT(String[] lines, List<string> client_data_list)
		{
			int num_PILOT = Get_num_clients(lines);
			foreach (var client_data in client_data_list)
			{
				var split_data = client_data.Split(':');
				if (split_data[3] == "ATC")
				{
					Debug.Log("接続されているクライアントはATCです");
					num_PILOT--;
					continue;
				}
			}
			return num_PILOT;
		}
	}
}