using Assets.src;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using Mathd;
using System.Xml.Serialization;
using static Assets.src.Brigde;
using System.Runtime.CompilerServices;

public class Airplanes : MonoBehaviour
{
    GameObject Brigde;
    GameObject ConvertPosition;
    Brigde brigde_tool;
    ConvertPosition ConvPos_tool;

    private float repeatSpan;
    private float timeElapsed;

    List<string> airplane_objList = new List<string>();

    // Start is called before the first frame update
    void Start()
    {   
        Brigde = GameObject.Find("Brigde");
        ConvertPosition = GameObject.Find("ConvPos");
        brigde_tool = Brigde.GetComponent<Brigde>();
        ConvPos_tool = ConvertPosition.GetComponent<ConvertPosition>();

        repeatSpan = 5;
        timeElapsed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= repeatSpan)
        {
            CreateAirplaneObject();
            timeElapsed= 0;
        }
    }
    void CreateAirplaneObject()
    {
        GameObject prefab_airplane = (GameObject)Resources.Load("Airplane");
        int num_Airplanes = 0;
        var lines = brigde_tool.Open_Whazzup();
        List<string> client_data_list = brigde_tool.Get_client_data(lines);

        List<Brigde.Airplane> airplanes = brigde_tool.GetAirplanes();

        foreach (Brigde.Airplane airplane in airplanes)
        {
            Vector2 Airplane_pos = ConvPos_tool.GetPos(airplane.lat, airplane.lon);

            Debug.Log(Airplane_pos);
            Debug.Log(brigde_tool.Get_num_PILOT(lines, client_data_list));
            Debug.Log(num_Airplanes);

            if (brigde_tool.Get_num_PILOT(lines, client_data_list) > num_Airplanes)
            {
                num_Airplanes++;
                GameObject airplane_obj = Instantiate(prefab_airplane, new Vector3(Airplane_pos.x,Airplane_pos.y), Quaternion.identity);
                airplane_obj.name = airplane.callsign;
                
                airplane_objList.Add(airplane_obj.name);
            }
        }
    }
}
